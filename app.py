import os
from flask import Flask, render_template, send_from_directory, redirect, url_for
from flask_talisman import Talisman
from settings import DEBUG, USE_DEBUGGER, USE_RELOADER

application = Flask(__name__)
csp = {
    'default-src': [
        '\'self\'',
        '*.googleapis.com',
        '*.mailchimp.com',
        '*.gstatic.com',
        '*.google-analytics.com',
        '*.googletagmanager.com',
        '*.amazon.com',
        '*.amazonaws.com',
        '*.cloudflare.com',
        '*.callrail.com',
        '*.googleadservices.com',
        '*.hotjar.com',
        '*.facebook.net',
        '*.trackcmp.net',
        '*.bing.net',
        '*.google.com',
        '*.facebook.com',
        '*.bing.com',
        '*.doubleclick.net',
        '\'unsafe-inline\'',
        'google.com',
        '*.bootstrapcdn.com',
        '*.hubspot.com',
        '*.hsforms.com',
    ],
    'script-src': [
        '\'self\'',
        '*.googleapis.com',
        '*.mailchimp.com',
        '*.gstatic.com',
        '*.google-analytics.com',
        '*.googletagmanager.com',
        '*.amazon.com',
        '*.amazonaws.com',
        '*.cloudflare.com',
        '*.callrail.com',
        '*.list-manage.com',
        '*.googleadservices.com',
        '*.hotjar.com',
        '*.facebook.net',
        '*.facebook.com',
        '*.trackcmp.net',
        'trackcmp.net',
        '*.bing.net',
        '*.bing.com',
        '*.doubleclick.net',
        '\'unsafe-inline\'',
        '\'unsafe-eval\'',
        '*.google.com',
        '*.jquery.com',
        '*.bootstrapcdn.com',
        '*.cloudfront.net',
        '*.activehosted.com',
        '*.app-us1.com',
        '*.hsforms.net',
        '*.hs-scripts.com',
        '*.hs-analytics.net',
        '*.hs-banner.com',        
        '*.hsforms.net',
        'js.hsleadflows.net',
        'forms.hsforms.com',
        '*.doubleclick.net',
        '*.google.com'
    ],
    'style-src': [
        '\'self\'',
        '*.googleapis.com',
        '*.mailchimp.com',
        '*.gstatic.com',
        '*.google-analytics.com',
        '*.googletagmanager.com',
        '*.amazon.com',
        '*.amazonaws.com',
        '*.cloudflare.com',
        '*.callrail.com',
        '\'unsafe-inline\'',
        '*.bootstrapcdn.com'
    ]
}
Talisman(application, force_https=False, content_security_policy=csp)
application.config.from_pyfile('settings.py')

@application.route("/")
def index():
    return render_template("index.html")

@application.route("/About")
def about():
    return render_template("about.html")

@application.route("/Sitemap")
def sitemap_list():
    return redirect(url_for('index'), code=301)

@application.route("/sitemap.xml")
def sitemap():
    return render_template("/sitemap.xml")

@application.route("/google404506f13c563966.html")
def GSC():
    return render_template("/google404506f13c563966.html")

@application.route("/google804d4f43bc2fc943.html")
def GSC_zuka():
    return render_template("/google804d4f43bc2fc943.html")

@application.route("/FL-LLC-Benefits")
def benefits():
    return render_template("FL-LLC-Benefits/index.html")

@application.route("/FL-LLC-Benefits/Annual-Report")
def ar():
    return render_template("/FL-LLC-Benefits/annual-report.html")

@application.route("/FL-LLC-Benefits/Free-IRS-EIN")
def ein():
    return render_template("/FL-LLC-Benefits/free-irs-ein.html")

@application.route("/FL-LLC-Benefits/Business-Bank-Account")
def bank():
    return render_template("/FL-LLC-Benefits/bank-account.html")

@application.route("/FL-LLC-Benefits/Holding-Company")
def re_holding():
    return redirect(url_for('holding'), code=301)

@application.route("/FL-Holding-Company")
def holding():
    return render_template("/holdingcompany/index.html")

@application.route("/FL-LLC-Benefits/Operating-Agreement")
def oa():
    return render_template("/FL-LLC-Benefits/oa.html")

@application.route("/FL-LLC-Benefits/Importance-of-Math-in-Business-for-Kids")
def math_kids():
    return render_template("/FL-LLC-Benefits/math.html")

@application.route("/FL-Holding-Company/Operating-Agreement")
def hc_oa():
    return render_template("/holdingcompany/operating-agreement.html")

@application.route("/Holding-Company/Operating-Agreement")
def re_hc_oa():
    return redirect(url_for('hc_oa'), code=301)

@application.route("/Holding-Company/Taxes")
def hc_taxes():
    return render_template("/holdingcompany/taxes.html")

@application.route("/Holding-Company/Real-Estate-Holding-Company")
def hc_real_estate():
    return render_template("/holdingcompany/real-estate.html")

@application.route("/FL-LLC-Benefits/Real-Estate-Holding-Company")
def re_real():
    return redirect(url_for('hc_real_estate'), code=301)

@application.route("/Holding-Company/Naming-Ideas")
def hc_naming():
    return render_template("/holdingcompany/naming.html")

@application.route("/FL-LLC-Benefits/Taxes")
def taxes():
    return render_template("/FL-LLC-Benefits/taxes.html")

@application.route("/FL-LLC-Benefits/Annual-Fees-And-Requirements")
def re_fees():
    return redirect(url_for('fees'), code=301)

@application.route("/FL-LLC-Benefits/Annual-Fees")
def fees():
    return render_template("/FL-LLC-Benefits/fees.html")

@application.route("/Florida-Land-Trust/Taxes")
@application.route("/FL-LLC-Benefits/Anonymous-Land-Trust")
def re_land():
    return redirect(url_for('land'), code=301)

@application.route("/Florida-Land-Trust")
def land():
    return render_template("/landtrust/index.html")

@application.route("/FL-LLC-Benefits/LLC-vs-Sole-Proprietorship")
def llcvssole():
    return render_template("/FL-LLC-Benefits/llc-vs-sole.html")

@application.route("/FL-LLC-Benefits/Single-Member-Limited-Liability-Company")
def smllc():
    return render_template("/FL-LLC-Benefits/sm-llc.html")

@application.route("/FL-LLC-Benefits/Single-vs-Multi-Member")
def re_smllc():
    return redirect(url_for('smllc'), code=301)

@application.route("/FL-LLC-Benefits/Forming-an-LLC-With-Your-Spouse")
def spouse():
    return render_template("/FL-LLC-Benefits/spouse.html")

@application.route("/FL-LLC-Benefits/Business-Judgement-Rule")
def judgement():
    return render_template("/FL-LLC-Benefits/judgement.html")

@application.route("/FL-LLC-Benefits/Fiduciary-Duties")
def fiduciary():
    return render_template("/FL-LLC-Benefits/fiduciary.html")

@application.route("/FL-LLC-Benefits/Best-State-to-Form-an-LLC")
def best():
    return render_template("/FL-LLC-Benefits/vs/index.html")

@application.route("/FL-LLC-Benefits/Articles-of-Organization")
def fl_articles():
    return render_template("/FL-LLC-Benefits/articles.html")

@application.route("/FL-LLC-Benefits/Manager-vs-Member-Managed")
def manager_vs_member():
    return render_template("/FL-LLC-Benefits/vs/manager-member.html")

@application.route("/FL-LLC-Benefits/vs-Nevada-LLC")
def vs_nevada():
    return render_template("/FL-LLC-Benefits/vs/vs-nevada.html")

@application.route("/FL-LLC-Benefits/vs-Professional-Association")
def vs_professional():
    return render_template("/FL-LLC-Benefits/vs/vs-professional.html")

@application.route("/FL-LLC-Benefits/vs-PLLC")
def vs_pllc():
    return render_template("/FL-LLC-Benefits/vs/vs-pllc.html")

@application.route("/FL-LLC-Benefits/vs-Partnership")
def vs_partnership():
    return render_template("/FL-LLC-Benefits/vs/vs-partnership.html")

@application.route("/FL-LLC-Benefits/vs-Wyoming-LLC")
def vs_wyoming():
    return render_template("/FL-LLC-Benefits/vs/vs-wyoming.html")

@application.route("/FL-LLC-Benefits/vs-S-Corp")
def vs_scorp():
    return render_template("/FL-LLC-Benefits/vs/vs-scorp.html")

@application.route("/FL-LLC-Benefits/vs-Delaware-LLC")
def vs_delaware():
    return render_template("/FL-LLC-Benefits/vs/vs-delaware.html")

@application.route("/FL-LLC-Benefits/Requirements")
def fl_reqs():
    return render_template("/FL-LLC-Benefits/requirements.html")

@application.route("/Florida-Registered-Agent")
def agent():
    return render_template("/Florida-Registered-Agent/index.html")

@application.route("/Registered-Agent-Services")
def agent_redirect():
    return redirect(url_for("agent"), code=301)

@application.route("/Florida-Registered-Agent/Registering-a-Foreign-Corporation")
def fcorp():
    return render_template("/Florida-Registered-Agent/registering-foreign-corp.html")

@application.route("/Florida-Registered-Agent/Foreign-LLC")
@application.route("/foreign-llc/")
def fllc_redirect():
    return redirect(url_for("fllc"), code=301)

@application.route("/Florida-Registered-Agent/Register-a-Foreign-LLC")
def fllc():
    return render_template("/Florida-Registered-Agent/foreign-llc.html")

@application.route("/Florida-Registered-Agent/FL-Secretary-of-State")
def secretary():
    return render_template("/Florida-Registered-Agent/secretary.html")

@application.route("/Florida-Registered-Agent/FL-Business-License")
def license():
    return render_template("/Florida-Registered-Agent/license.html")

@application.route("/Florida-Registered-Agent/FL-LLC-Name-Search")
def name_search():
    return render_template("/Florida-Registered-Agent/name-search.html")

@application.route("/Florida-Registered-Agent/How-to-File-a-FL-DBA")
def file_dba():
    return render_template("/Florida-Registered-Agent/dba.html")

@application.route("/Florida-Registered-Agent/How-to-Dissolve-a-FL-LLC")
def dissolve_llc():
    return render_template("/Florida-Registered-Agent/dissolve-llc.html")

@application.route("/Florida-Registered-Agent/Museums-Landmarks-and-Theme-Parks-of-Florida")
def museums():
    return render_template("/Florida-Registered-Agent/museums-parks.html")

@application.route("/Florida-Corporation")
def corp():
    return render_template("/FL-Corporation/index.html")




@application.route("/Florida-Corporation/Articles-of-Incorporation")
def corp_aoi():
    return render_template("/FL-Corporation/corp_aoi.html")

@application.route("/Florida-Corporation/Bylaws")
def corp_bylaws():
    return render_template("/FL-Corporation/corp_bylaws.html")


@application.route("/Florida-Corporation/S-Corp-Taxation")
def corp_taxation():
    return render_template("/FL-Corporation/corp_taxation.html")


@application.route("/Florida-Corporation/Non-Profit")
def corp_no_profit():
    return render_template("/FL-Corporation/corp_no_profit.html")


@application.route("/Florida-Land-Trust/vs-Living-Trust")
def corp_vlt():
    return render_template("/landtrust/vslivingtrust.html")


@application.route("/Florida-Land-Trust/Trustee")
def corp_trustee():
    return render_template("/landtrust/trustee.html")


@application.route("/Florida-Land-Trust/vs-Personal-Property-Trust")
def corp_ppt():
    return render_template("/landtrust/ppt.html")

@application.route("/Florida-Land-Trust/LLC-Trustee")
def corp_llc_trustee():
    return render_template("/landtrust/llctrustee.html")

@application.route("/Florida-Land-Trust/Conservation-Land-Trust")
def conservation_lt():
    return render_template("/landtrust/conservation.html")

@application.route("/Florida-Land-Trust/Asset-Protection-Trust")
def apt():
    return render_template("/landtrust/apt.html")

@application.route("/Florida-Land-Trust/Land-Conservation")
def land_conservation():
    return render_template("/landtrust/land-cons.html")

@application.route("/Florida-Corporation/How-to-Dissolve-a-FL-Corporation")
def fl_dissolve():
    return render_template("/FL-Corporation/dissolve.html")

@application.route("/Florida-Corporation/FL-Corporation-Name-Search")
def fl_ns():
    return render_template("/FL-Corporation/name-search.html")

@application.route("/Florida-Virtual-Office")
def vo():
    return render_template("/Virtual-Office/index.html")

@application.route("/Florida-Virtual-Office/Mail-Scanning")
def mail():
    return render_template("/Virtual-Office/mail.html")

@application.route("/Florida-Virtual-Office/Phone")
def phone():
    return render_template("/Virtual-Office/phone.html")

@application.route("/Florida-Virtual-Office/Phone-Forwarding-and-Answering")
def forwarding_answering():
    return render_template("/Virtual-Office/forwarding-answering.html")

@application.route("/Florida-Virtual-Office/Virtual-Office-Pros-and-Cons")
def vo_pros_cons():
    return render_template("/Virtual-Office/vo-pros-cons.html")

@application.route("/Florida-Virtual-Office/Resources-for-Building-a-Business-Website")
def vo_business_web():
    return render_template("/Virtual-Office/business-website.html")

@application.route("/Anonymous-LLC")
def anonymous():
    return render_template("/FL-Double-LLC/index.html")

@application.route("/Florida-Bookkeeping")
def bookkeeping():
    return render_template("/FL-Bookkeeping/index.html")

@application.route("/Forming-an-LLC-in-Texas")
def tx_index():
    return render_template("/tx/index.html")

@application.route("/ui9xpddqm9sk1qab0fwqclm5vmxvrw.html")
def fl_file():
    return render_template("/Florida.html")

@application.route("/Terms")
def terms():
    return render_template('terms-and-conditions.html')

@application.route("/Privacy")
def privacy():
    return render_template('privacy-notice.html')

@application.route("/Contact-Test724MAP")
def contact_test():
    return render_template('contact-test.html')

@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

if __name__ == '__main__':
    application.run(debug=True, use_debugger=True, use_reloader=True)