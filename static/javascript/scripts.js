

// dropdown
const element = document.querySelectorAll('.dropdown-toggle')
const all_dropdown = document.querySelectorAll('.dropdown-menu')
const closeBtn = document.querySelectorAll('.dropdown-menu .close')

// close mobile sub dropdown
closeBtn.forEach(function(dr) {
  dr.addEventListener('click', function(e) {
    e.classList.remove('show')
  })
})

// open dropdown (toggle)
element.forEach(function (el) {

  if(window.innerWidth > 1100) {
    // el.addEventListener('mouseenter', function (e) {
    //   if(el.parentElement.contains(e.target)) {
    //     console.log(el.parentElement)
    //     console.log(el.parentElement.contains(e.target))
    //     this.parentElement.classList.add('show')
    //   }
    // })
  
    // el.addEventListener('mouseleave', function(e) {
  
    //   if(el.parentElement.contains(e.target)) {
    //     console.log(el.parentElement)
    //     console.log(el.parentElement.contains(e.target))
    //     this.parentElement.classList.remove('show')
    //   }
    // })
  } else {
    el.addEventListener('click', function (e) {
      if(el.contains(e.target)) {
        this.parentElement.querySelector('.dropdown-menu').classList.toggle('show')
      }
    })
  }

})

// close dropdown menu if the user click outside of it
window.onclick = function (e) {
  if (!e.target.matches('.dropdown-toggle')) {
    all_dropdown.forEach(function (menu) {
      if (menu.classList.contains('show')) {
        menu.classList.remove('show')
      }
    })
    element.forEach(function(drop) {
      if(drop.getAttribute('aria-expanded') === 'true') {
        drop.setAttribute('aria-expanded', 'false')
      }
    })
  }
}

// responsive burger menu open
const toggler = document.querySelector('.navbar-toggler')

toggler.addEventListener('click', function (el) {

  const parent = this.parentElement
  const burger_menu = parent.querySelector('.navbar-collapse')

  burger_menu.classList.toggle('show')
})


// accordion
const accordion = document.querySelectorAll('.accordion .item-header')

accordion.forEach(function(element) {

  element.addEventListener('click', e => {

    const parent = e.target.parentElement

    let isOpen = e.target.parentElement.attributes['open'].textContent

    if(isOpen === 'true') {
      isOpen = false
      parent.setAttribute('open', false)
    } else {
      isOpen = true
      parent.setAttribute('open', true)
    }

  })

})


// scroll event
document.addEventListener('scroll', function(e) {
  if(window.scrollY > 0) {
    document.querySelector('.navbar').classList.add('sticky')
  } else {
    document.querySelector('.navbar').classList.remove('sticky')
  }
})

window.addEventListener('load', function(e) {
  if(window.scrollY > 0) {
    document.querySelector('.navbar').classList.add('sticky')
  } else {
    document.querySelector('.navbar').classList.remove('sticky')
  }
})

const indicator = document.querySelector('.contact-circle')
const contactForm = document.querySelector('.contact-form')

const closeIcon = indicator.children.item(0)
const openIcon = indicator.children.item(1)

let isOpen = false

indicator.addEventListener('click', e => {

    if(isOpen == true) {
        isOpen = false

        closeIcon.classList.remove('d-none')
        openIcon.classList.add('d-none')

        contactForm.classList.remove('show')
    } else {
        isOpen = true

        closeIcon.classList.add('d-none')
        openIcon.classList.remove('d-none')

        contactForm.classList.add('show')
    }

})

const contactIndicator = document.querySelectorAll('[contact="true"]')
contactIndicator.forEach(e => {
    e.addEventListener('click', btn => {
        btn.preventDefault()

        isOpen = true

        closeIcon.classList.add('dnone')
        openIcon.classList.remove('dnone')

        contactForm.classList.add('show')
    })
})